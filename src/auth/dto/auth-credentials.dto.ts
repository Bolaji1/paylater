import { IsString, MinLength, MaxLength, Matches } from 'class-validator';
export class AuthCredentialsDto {

  @IsString()
  @MinLength(3)
  @MaxLength(20)
  username: string;

  @IsString()
  @MinLength(8)
  @MaxLength(20)
  @Matches(
    /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
    { message: 'Password too weak'},
    ) // at least one uppercase, at least one 1, special character
  password: string;
}
