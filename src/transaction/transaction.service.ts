import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { GetBvnReport } from '../credit-check/dto/get-bvn-report.dto';
import { Transaction } from './transaction.entity';
import { TransactionRepository } from './transaction.respository';
import { GetLoanCalculationDto } from './dto/get-loan-calculations-dto';

@Injectable()
export class TransactionService {

   constructor(
    @InjectRepository(Transaction)
    private transactionRepository: TransactionRepository,
   ){}
  async createBlankTransaction( bvnReport: GetBvnReport) {
      const blankTransaction = await this.transactionRepository.createBlankTransaction(bvnReport);
      
      return blankTransaction;
  }  

  async getLoanCalculations(getLoanCalculationDto: GetLoanCalculationDto) {
    const { requested_amount, loan_terms, payment_type} = getLoanCalculationDto;
    const vPrincipal = parseInt(requested_amount, 10);
    const vLoanfees = 0;
    const vSalestax = 0; //sn(document.calc.salestax.value)/100;
    const vPrice = 0;
    let interestRate = 10;
    let vSalestaxamount;
    let loanTermsInMonths = parseInt(loan_terms, 10);
    let vPPY = 12;
    let vloantermtype = 12;
    let vtradein = 0; 
    let vdown = 0;
    //let Vdownpay = price * vdown * 0.01; 
    let vDownpay = 0;
    let numberOfPayments;
    let interest;

    
     // ok
     let vPaymentio;
     let vPaymentiototal;
     let vTotalrepayio;
     let vPayment;
     let vTotalint;
     let vTotalrepay;
     let vfullcost;

    if(vPrice === 0){
      vSalestaxamount = vSalestax * vPrincipal;
    }else {
      vSalestaxamount = vSalestax * vPrice;
    }

    if(vPrincipal === 0 || loanTermsInMonths === 0){
      return ({
        regular_payment_amount: fns(0),
        total_interest_paid: fns(0),
        principal_plus_interest: fns(0),
        number_of_payments: 0,
      })
    }else {
      if(loanTermsInMonths === 0){

      }else {
        interest = interestRate / 100.0;
        numberOfPayments = Math.round(loanTermsInMonths * vPPY / vloantermtype);

        if (numberOfPayments == 0) {
          numberOfPayments = 1;
        }

        interest /= vPPY;

        if(interest === 0){
           // nothing
        }else {
          vPaymentio = vPrincipal * interest;
          vPaymentiototal = vPaymentio * vPPY * loanTermsInMonths / vloantermtype;
          vTotalrepayio = vPaymentiototal + vPrincipal;

          var pow = 1;
          for(var j = 0; j < numberOfPayments; j++) {
             pow = pow * (1 + interest);
          }

          vPayment= (vPrincipal * pow * interest) / (pow - 1);
          vTotalint = Number(vPayment * numberOfPayments) - Number(vPrincipal);
          vTotalrepay = Number(vPrincipal) + Number(vTotalint);
          vfullcost = Number(vTotalrepay) + Number(vtradein) +Number(vDownpay);
        }
      }
    }
   
    return ({
      regular_payment_amount: fns(vPayment),
      total_interest_paid: fns(vTotalint),
      principal_plus_interest: fns(vTotalrepay),
      number_of_payments: numberOfPayments,
    });
  }
}

function fns(num, places = 2, comma = 1, type = 1, show =1) {

  let sym_1 = "₦";
  let sym_2 = ""; 
  let onum;
  let integer;
  let decimal;
  let fillZeroes;
  let finNum;

  let isNeg=0;

  if(num < 0) {
     num=num*-1;
     isNeg=1;
  }

  let myDecFact = 1;
  let myPlaces = 0;
  let myZeros = "";
  while(myPlaces < places) {
     myDecFact = myDecFact * 10;
     myPlaces = Number(myPlaces) + Number(1);
     myZeros = myZeros + "0";
  }
  
onum=Math.round(num*myDecFact)/myDecFact;
  
integer=Math.floor(onum);

if (Math.ceil(onum) == integer) {
  decimal=myZeros;
} else{
  decimal=Math.round((onum-integer)* myDecFact)
}
decimal=decimal.toString();
if (decimal.length<places) {
      fillZeroes = places - decimal.length;
   for (let z=0;z<fillZeroes;z++) {
      decimal="0"+decimal;
      }
   }

 if(places > 0) {
    decimal = "." + decimal;
 }

 if(comma == 1) {
integer=integer.toString();
let tmpnum="";
let tmpinteger="";
let y=0;

for (let x=integer.length;x>0;x--) {
  tmpnum=tmpnum+integer.charAt(x-1);
  y=y+1;
  if (y === 3 && x>1) {
    tmpnum=tmpnum+",";
    y=0;
  }
}

for (let x=tmpnum.length;x>0;x--) {
  tmpinteger=tmpinteger+tmpnum.charAt(x-1);
}


finNum=tmpinteger+""+decimal;
 } else {
    finNum=integer+""+decimal;
 }

  if(isNeg == 1) {
     if(type == 1 && show == 1) {
        finNum = "-" + sym_1 + "" + finNum + "" + sym_2;
     } else {
        finNum = "-" + finNum;
     }
  } else {
     if(show == 1) {
        if(type == 1) {
           finNum = sym_1 + "" + finNum + "" + sym_2;
        } else
        if(type == 2) {
           finNum = finNum + "%";
        }

     }

  }

return finNum;
}