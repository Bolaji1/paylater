import { EntityRepository, Repository } from 'typeorm';
import { ConflictException, InternalServerErrorException, Logger } from '@nestjs/common';
import { GetBvnReport } from '../credit-check/dto/get-bvn-report.dto';
import { Transaction } from './transaction.entity';
import * as uuid from 'uuid';

@EntityRepository(Transaction)
export class TransactionRepository extends Repository<Transaction> {
    private logger = new Logger('TransactionRepository');
    constructor() {
        super();
      }

      async createBlankTransaction(bvnReport: GetBvnReport): Promise<any> {
          const transaction = new Transaction();
          const {
            bvn,
            first_name,
            middle_name,
            last_name,
            date_of_birth,
            phone_number,
            gender,
          } = bvnReport;

          transaction.bvn = bvn;
          transaction.transactionGuid = uuid.v4();
          transaction.firstName = first_name;
          transaction.lastName = last_name;
          transaction.middleName = middle_name;
          transaction.dob = date_of_birth;
          transaction.phoneNumber = phone_number;
          transaction.gender = gender;
          transaction.loanAmount = 0;
          transaction.annualInterest = 0;
          transaction.loanAmount = 0;
          transaction.monthlyInterest = 0;
          transaction.repaymentPeriod = 0;
          transaction.monthlyPayment = 0;
          transaction.totalPayment = 0;
          transaction.totalInterest = 0;

    try {
      const { transactionGuid } = await transaction.save();
      
      return ({ success: true, transactionGuid});
      
    } catch (error) {
      // duplicate transaction
      if (error.code === '23505') {
        throw new ConflictException('Transaction already exists');
      } else {
          this.logger.log(JSON.stringify(error))
        throw new InternalServerErrorException();
      }
    }

      }
}