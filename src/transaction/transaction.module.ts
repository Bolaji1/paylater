import { Module } from '@nestjs/common';
import { TransactionController } from './transaction.controller';
import { TransactionService } from './transaction.service';
import { TransactionRepository } from './transaction.respository';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  controllers: [TransactionController],
  providers: [TransactionService],
  imports: [
    TypeOrmModule.forFeature([TransactionRepository] )
  ]
})
export class TransactionModule {}
