import { IsString, MinLength, MaxLength, IsNumber } from 'class-validator';
export class CreateTransactionDto {

  /* Personal info */
  @IsString()
  @MinLength(11)
  bvn: string;

  @IsString()
  email: string;

  @IsString()
  @MinLength(2)
  @MaxLength(20)
  firstName: string;

  @IsString()
  @MinLength(2)
  @MaxLength(20)
  lastName: string;

  @IsString()
  dob: string;
  @IsString()
  @MinLength(8)
  phoneNumber: string;

  /* Account Info */
  @IsString()
  accountNumber: string;

  /* Address info */
  @IsString()
  location: string;

  @IsString()
  street: string;

  @IsString()
  city: string;

   /* Finance Info */
  @IsNumber()
  loanAmount: number;

  @IsNumber()
  annualInterest: number;

  @IsNumber()
  monthlyInterest: number;

}
