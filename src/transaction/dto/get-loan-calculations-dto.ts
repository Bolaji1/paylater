import { IsString, MinLength,  } from 'class-validator';

export class GetLoanCalculationDto {

  @IsString()
  requested_amount: string;

  @IsString()
  loan_terms: string;

  @IsString()
  @MinLength(2)
  payment_type: string;

}
