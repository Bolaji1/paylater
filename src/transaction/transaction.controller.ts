import { Controller, UseGuards, Post, ValidationPipe, Body, Logger } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from '../auth/get-user.decorator';
import { User } from '../auth/user.entity';
import { CreateTransactionDto } from './dto/create-transaction-dto';
import { GetLoanCalculationDto } from './dto/get-loan-calculations-dto';
import { TransactionService } from './transaction.service'

@Controller('api/transaction')
@UseGuards(AuthGuard('jwt'))
export class TransactionController {
  private logger = new Logger('TransactionController');
  constructor(private transactionService: TransactionService){

  }

  @Post('/create-account')
  checkAccount(@GetUser() user: User, @Body(ValidationPipe) createTransactionDto: CreateTransactionDto) {
        console.log(createTransactionDto)
      //return this.creditCheckService.checkCredit(createAccountDto);
  }

  @Post('/get-loan-calculations')
  getLoanDetails(@GetUser() user: User, @Body(ValidationPipe) getLoanCalculationDto: GetLoanCalculationDto) {
    this.logger.log(`Getting loan calculation for user:${user.username}`);

      return this.transactionService.getLoanCalculations(getLoanCalculationDto);

  }
}
