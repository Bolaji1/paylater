import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, Unique, CreateDateColumn, UpdateDateColumn } from 'typeorm';

@Entity()
@Unique(['email', 'phoneNumber', 'bvn'])
export class Transaction extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true})
  email: string;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  middleName: string;

  @Column()
  dob: string;

  @Column()
  phoneNumber: string;

  @Column()
  gender: string;

  @Column({ nullable: true})
  accountNumber: number;

  @Column({ nullable: true})
  location: string;

  @Column({ nullable: true})
  street: string;

  @Column({ nullable: true})
  city: string;

  @Column({ nullable: true})
  loanAmount: number;

  @Column({ nullable: true})
  annualInterest: number;

  @Column({ nullable: true})
  monthlyInterest: number;

  @Column({ nullable: true})
  repaymentPeriod: number;

  @Column({ nullable: true})
  monthlyPayment: number;

  @Column({ nullable: true})
  totalPayment: number;

  @Column({ nullable: true})
  totalInterest: number;

  @Column({ nullable: true})
  note: string;

  @Column({ nullable: true})
  bvn: string;

  @Column({ nullable: true})
  transactionGuid: string;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;

}
