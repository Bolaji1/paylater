import { Injectable, HttpService, Logger, NotFoundException } from '@nestjs/common';
import { CreditCheckDto } from './dto/credit-check-dto';
import * as config from 'config';
import { GetBvnReport } from './dto/get-bvn-report.dto';
import { TransactionService } from 'src/transaction/transaction.service';

@Injectable()
export class CreditCheckService {
  private logger = new Logger('CreditCheckService');
  flutterConfig: any;

  constructor(private readonly httpService: HttpService, private transactionService: TransactionService) {
    this.flutterConfig = config.get('flutter');
  }

  async checkCredit(creditCheckDto: CreditCheckDto) {
    const { bvn } = creditCheckDto;

    try {
      const bvnReport: GetBvnReport = await this.validateBVN(bvn);
      console.log(bvnReport)
      const canFinance = this.validateUserDetails(creditCheckDto, bvnReport);
      let blankTransaction = {};
      
      if(canFinance){
         blankTransaction = await this.transactionService.createBlankTransaction(bvnReport);
      
      }
  
      return ({  canFinance, ...blankTransaction })

    } catch (e) {
      throw new NotFoundException('Failed to get bvn report:');
    }

  }

  private validateUserDetails(creditCheckDto: CreditCheckDto, bvnReport: GetBvnReport) {
    const { bvn, firstName, lastName, dob, phoneNumber } = creditCheckDto;
    const validFirstName = this.checkParameter(bvnReport.first_name, firstName);
    const validLastName = this.checkParameter(bvnReport.last_name, lastName);
    const validDOB = this.checkParameter(bvnReport.date_of_birth, dob);
    const validBVN = this.checkParameter(bvnReport.bvn, bvn);
    const validPhoneNumber = this.checkParameter(bvnReport.phone_number, phoneNumber);

    return (validFirstName && validLastName && validDOB && validBVN && validPhoneNumber);

  }

  private checkParameter(paramFromBvnReport, paramFromUser){
    return  paramFromBvnReport.toLowerCase() === paramFromUser.toLowerCase();
  }

  private validateBVN(bvn: string): Promise<any> {
    const { bvn_check_url, api_key } = this.flutterConfig;
    return new Promise((resolve, reject) => {
      this.httpService.get(`${bvn_check_url}/${bvn}?seckey=${api_key}`)
        .subscribe(({data, status}) => {
          resolve(data.data || {});
        }, (e) => {
          this.logger.error(`Failed to get bvn report: ${bvn}.`, e.stack);
          reject(e);
        });
    });

  }
}
