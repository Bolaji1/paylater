import { IsString, MinLength, MaxLength } from 'class-validator';

export class GetBvnReport {

  @IsString()
  @MinLength(11)
  bvn: string;

  @IsString()
  first_name: string;

  @IsString()
  last_name: string;

  @IsString()
  middle_name: string;

  @IsString()
  date_of_birth: string;

  @IsString()
  phone_number: string;

  @IsString()
  registration_date: string;

  @IsString()
  enrollment_bank: string;

  @IsString()
  enrollment_branch: string;

  @IsString()
  gender: string;

  @IsString()
  nationality: string;

  @IsString()
  state_of_residence: string;

  @IsString()
  lga_of_residence: string;

  @IsString()
  image: string;
}
