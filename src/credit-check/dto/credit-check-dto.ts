import { IsString, MinLength, MaxLength } from 'class-validator';
export class CreditCheckDto {

  @IsString()
  @MinLength(11)
  bvn: string;

  @IsString()
  @MinLength(2)
  @MaxLength(20)
  firstName: string;

  @IsString()
  @MinLength(2)
  @MaxLength(20)
  lastName: string;

  @IsString()
  dob: string;


  @IsString()
  @MinLength(8)
  phoneNumber: string;
}
