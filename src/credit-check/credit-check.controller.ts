import { Body, Controller, Post, UseGuards, ValidationPipe } from '@nestjs/common';
import { CreditCheckDto } from './dto/credit-check-dto';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from '../auth/get-user.decorator';
import { User } from '../auth/user.entity';
import { CreditCheckService } from './credit-check.service';

@Controller('api/credit-check')
@UseGuards(AuthGuard('jwt'))
export class CreditCheckController {

  constructor(private creditCheckService: CreditCheckService) {
  }

  @Post()
  checkCredit(@GetUser() user: User, @Body(ValidationPipe) creditCheckDto: CreditCheckDto) {
    return this.creditCheckService.checkCredit(creditCheckDto);
  }
}
