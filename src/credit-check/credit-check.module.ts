import { HttpModule, Module } from '@nestjs/common';
import { CreditCheckService } from './credit-check.service';
import { CreditCheckController } from './credit-check.controller';
import { TransactionService } from '../transaction/transaction.service';
import { TransactionRepository } from '../transaction/transaction.respository';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [HttpModule,     TypeOrmModule.forFeature([TransactionRepository] )],
  providers: [CreditCheckService, TransactionService],
  controllers: [CreditCheckController],
})
export class CreditCheckModule {}
