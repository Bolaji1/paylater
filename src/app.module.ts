import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { typeOrmConfig } from './config/typeorm.config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CreditCheckModule } from './credit-check/credit-check.module';
import { TransactionModule } from './transaction/transaction.module';

@Module({
  imports: [AuthModule, CreditCheckModule, TransactionModule,  TypeOrmModule.forRoot(typeOrmConfig)],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
