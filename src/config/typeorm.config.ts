import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import * as config from 'config';

const dbConfig = config.get('db');

export const typeOrmConfig: TypeOrmModuleOptions = {
  type: dbConfig.type,
  host: process.env.DATABASE_HOSTNAME || dbConfig.host,
  password:  process.env.DATABASE_PASSWORD || dbConfig.password,
  database: process.env.DATABASE_NAME || dbConfig.database,
  username: process.env.DATABASE_USERNAME || dbConfig.username,
  entities: [__dirname + '/../**/*.entity.{js,ts}'],
  port: dbConfig.port,
  ssl: dbConfig.ssl,
  synchronize: dbConfig.synchronize,

}
